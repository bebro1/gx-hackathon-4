# Overview of Hackathon #4

![image.png](https://gaia-x.eu/sites/default/files/styles/800x425/public/2022-06/GX-Hackathon%234_Visual_Eventbild_Website_800x425px.png?itok=UMg3UdWK)

We are delighted to invite the Gaia-X community to the fourth official Gaia-X Hackathon. **The Hackathon will take place June 20-21, 2022**

The Gaia-X Hackathon is a two days event with each day being crowned with a presentation session of showcases that leverage components and concepts of the Gaia-X architecture. If you would like to participate in the Hackathon, please **register** via the following link:

https://gaia-x.eu/news/events/hackathon-4

The Gaia-X Hackathon will be organized as a community event in regular intervals by the Open Work Package Minimal Viable Gaia-X/Piloting.

# Getting in Touch

For discussions related to the Hackathon you can use the mailing list of the MVG/Piloting Open Work Package. You can subscribe to it via the following link:
https://list.gaia-x.eu/postorius/lists/wp-minimal-viable-gaia.list.gaia-x.eu/

**If you would like to participate in the organization and/or would like to get updates on the organization, please join our call on:**
- Thursday 9 a.m. CET - join: [MS Teams Link](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NWIwNTVmOTEtMDIwNi00N2RjLTgxOTUtZWVhYTkzNTgwZmEy%40thread.v2/0?context=%7b%22Tid%22%3a%22e3386221-1143-4129-acd3-cfcb8a6fcc7f%22%2c%22Oid%22%3a%220838870d-2d9b-49c1-a396-4cdb76099e06%22%7d)


# Schedule and Further Information

You'll find the **schedule** and wiki right [here](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-4/-/wikis/GX-Hackathon-4).

Please find and add session proposals here: **[Hackathon 4 Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-4/-/wikis/Hackathon-4-Proposals)**
